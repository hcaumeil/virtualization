# Copyright 2017 Bo Ørsted Andresen
# Distributed under the terms of the GNU General Public License v2

require github [ project=Winetricks ]

SUMMARY="Winetricks is an easy way to work around problems in Wine"
DESCRIPTION="
It has a menu of supported games/apps for which it can do all the workarounds automatically. It
also allows the installation of missing DLLs and tweaking of various Wine settings.
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# requires X - could use xvfb
RESTRICT="test"

DEPENDENCIES="
    run:
        app-arch/cabextract [[ note = [ Required by some verbs ] ]]
        app-arch/p7zip [[ note = [ Required by some verbs ] ]]
        app-arch/unrar [[ note = [ Required by some verbs ] ]]
        app-arch/unzip [[ note = [ Required by some verbs ] ]]
        app-arch/xz [[ note = [ Required by some verbs ] ]]
        dev-lang/perl:* [[ note = [ Used to munge steam config files ] ]]
    recommendation:
        app-emulation/wine [[ description = [ Winetricks is mainly a tool for configuring Wine ] ]]
        gnome-desktop/zenity [[ description = [ Recommended GUI for winetricks --gui ] ]]
    suggestion:
        app-admin/sudo [[ description = [ Mount .iso images if the user cached them with -k option ] ]]
        kde/kdialog [[ description = [ Alternative GUI for winetricks --gui ] ]]
        sys-auth/polkit:1 [[ description = [ Mount .iso images if the user cached them with -k option ] ]]
        sys-fs/archivemount [[ description = [ FUSE-based ISO mounting/unmounting ] ]]
        sys-fs/fuseiso [[ description = [ FUSE-based ISO mounting/unmounting ] ]]
        x11-apps/xdg-utils [[ description = [ Open download pages when the fetch cannot be fully automated ] ]]
"

DEFAULT_SRC_INSTALL_PARAMS=( PREFIX=/usr/$(exhost --target) )

WORK=${WORKBASE}/${PNV}

src_prepare() {
    # TODO: fix upstream
    edo sed \
        -e 's:$(PREFIX)/share:/usr/share:g' \
        -i Makefile

    default
}

pkg_postinst() {
    elog 'To run with wine32 run `WINE=wine32 winetricks`'
    elog 'To run with wine64 run `WINE=wine64 winetricks`'
}

