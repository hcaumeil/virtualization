# Copyright 2020 Maxime Sorin <maxime.sorin@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='docker' project='cli' tag=v${PV} ]
require bash-completion zsh-completion

SUMMARY="The home of the cli used in the Docker CE and Docker EE products"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/go[>=1.21.11]
"

RESTRICT="test strip"

GOWORK=src/github.com/docker

src_prepare() {
    default

    edo mkdir -p ${GOWORK}
    edo ln -s "${WORK}" ${GOWORK}/cli
}

src_compile() {
    local cli_gopath
    cli_gopath="${WORK}"

    LDFLAGS="" DISABLE_WARN_OUTSIDE_CONTAINER=1 GOPATH=${cli_gopath} VERSION=${PV} GITCOMMIT=${PV} emake dynbinary
}

src_install() {
    newbin build/docker docker

    dobashcompletion contrib/completion/bash/docker docker
    dozshcompletion contrib/completion/zsh/_docker

    emagicdocs
}

