# Copyright 2012-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="X.Org QXL video driver & Xspice"
DESCRIPTION="
Xspice is an X server and Spice server in one. It consists of a wrapper script
for executing Xorg with the right parameters and environment variables, a
module names spiceqxl_drv.so implementing three drivers: a video mostly
code identical to the guest qxl X driver, and keyboard and mouse reading from
the spice inputs channel.
"
HOMEPAGE="https://www.spice-space.org"
DOWNLOADS="https://xorg.freedesktop.org/releases/individual/driver/${PNV}.tar.xz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtualization-lib/spice-protocol[>=0.12.2]
        x11-proto/xorgproto
        x11-utils/util-macros[>=1.4]
    build+run:
        sys-apps/systemd[>=182]
        virtualization-lib/spice[>=0.13.2]
        x11-dri/libdrm[>=2.4.46]
        x11-libs/libXfont2
        x11-libs/libpciaccess[>=0.10]
        x11-libs/pixman:1
        x11-server/xorg-server[>=1.0.99.901]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-kms
    --enable-xspice
    --disable-ccid
    --disable-selective-werror
)

