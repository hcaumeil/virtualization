# Copyright 2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mingw64-toolchain-11.0.0_p2.ebuild', which is:
#     Copyright 2022-2024 Gentoo Authors

BINUTILS_PV=2.42
GCC_PV=14.1.0
MINGW_PV=$(ever range 1-3)

require sourceforge [ project=mingw-w64 pn=mingw-w64 pv=v${PV} suffix=tar.bz2 ]
require flag-o-matic

SUMMARY="All-in-one mingw64 toolchain intended for building Wine without multiarch"
HOMEPAGE+=" https://www.mingw-w64.org"
DOWNLOADS+="
    mirror://gnu/binutils/binutils-${BINUTILS_PV}.tar.xz
    mirror://gnu/gcc/gcc-${GCC_PV}/gcc-${GCC_PV}.tar.xz
"

LICENCES="
    GPL-3 [[ note = [ binutils and gcc ] ]]
    || ( GPL-3 LGPL-3 ) [[ note = [ gcc libraries ] ]]
    BSD-1 BSD-2 ISC LGPL-2 LGPL-2.1 MIT public-domain ZPL-2.1 [[ note = [ mingw64-runtime ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="strip test"

DEPENDENCIES="
    build+run:
        dev-libs/gmp:=
        dev-libs/mpc:=
        dev-libs/mpfr:=
        sys-libs/zlib
"

WORK=${WORKBASE}

src_prepare() {
    # rename directories to simplify both patching and the exheres
    edo mv binutils{-${BINUTILS_PV},}
    edo mv gcc{-${GCC_PV},}
    edo mv mingw-w64-v${MINGW_PV} mingw64

    default
}

src_compile() {
    # not great but do everything in src_compile given bootstrapping
    # process needs to be done in steps of configure+compile+install
    # (done modular to have most package-specific things in one place)

    CTARGET=x86_64-w64-mingw32

    MWT_D=${TEMP}/root # moved to ${IMAGE} in src_install
    local mwtdir=/usr/$(exhost --target)/lib/${PN}
    local prefix=${mwtdir}
    local sysroot=${MWT_D}${prefix}
    local -x PATH=${sysroot}/bin:${PATH}
    local multilib=true

    # global configure flags
    local conf=(
        --build=${CBUILD:-${CHOST}}
        --target=${CTARGET}
        --{doc,info,man}dir=/.skip # let the real binutils+gcc handle docs
        MAKEINFO=: # (bgo #922230)
    )

    # binutils
    local conf_binutils=(
        --prefix="${prefix}"
        --host=${CHOST}
        --disable-cet
        --disable-default-execstack
        --disable-nls
        --disable-shared
        --with-system-zlib
        --without-debuginfod
        --without-msgpack
        --without-zstd
    )
    mwt-binutils() {
        # symlink gcc's lto plugin for AR (bgo #854516)
        edo ln -s ../../libexec/gcc/${CTARGET}/${GCC_PV%%[.-]*}/liblto_plugin.so \
            "${sysroot}"/lib/bfd-plugins
    }

    # gcc (minimal -- if need more, disable only in stage1 / enable in stage3)
    local conf_gcc=(
        --prefix="${prefix}"
        --host=${CHOST}
        --disable-bootstrap
        --disable-cet
        --disable-gcov #843989
        --disable-gomp
        --disable-libquadmath
        --disable-libsanitizer
        --disable-libssp
        --disable-libvtv
        --disable-shared
        --disable-werror
        --with-gcc-major-version-only
        --with-system-zlib
        --without-isl
        --without-zstd
    )

    local conf_gcc_stage1=(
        --enable-languages=c
        --disable-libatomic
        --with-sysroot="${sysroot}"
    )
    local -n conf_gcc_stage2=conf_gcc_stage1

    local conf_gcc_stage3=(
        --enable-languages=c,c++
        --enable-threads=posix # needs stage3, and is required for dxvk/vkd3d
        --with-sysroot="${prefix}"
        --with-build-sysroot="${sysroot}"
    )

    # mingw64-runtime (split in several parts, 3 needed for gcc stages)
    local conf_mingw64=(
        --prefix="${prefix}"/${CTARGET}
        --host=${CTARGET}
        --with-sysroot=no
        --without-{crt,headers}

        # mingw .dll aren't used by wine and packages wouldn't find them
        # at runtime, use crossdev if need dll and proper search paths
        --disable-shared
    )

    local conf_mingw64_headers=(
        --enable-idl
        --with-headers

        # ucrt has not been tested and migration needs looking into, force
        # msvcrt-os for now (if really want this either use crossdev or
        # override using MWT_MINGW64_{HEADERS,RUNTIME}_CONF=... env vars)
        --with-default-msvcrt=msvcrt-os
    )
    mwt-mingw64_headers() { edo ln -s ${CTARGET} "${sysroot}"/mingw; } # (bgo #419601)

    local conf_mingw64_runtime=(
        --with-crt
        --with-default-msvcrt=msvcrt-os # match with headers
    )

    local conf_mingw64_libraries=( --with-libraries )
    local conf_mingw64_libraries32=(
        --libdir="${prefix}"/${CTARGET}/lib32
        --with-libraries
        CC="${CTARGET}-gcc -m32"
        RCFLAGS="--target=pe-i386 ${RCFLAGS}"
    )

    # mingw64-runtime's idl compiler (useful not to depend on wine for widl)
    local conf_widl=( --prefix="${prefix}" )

    # mwt-build [-x] <path/package-name> [stage-name]
    # -> ./configure && make && make install && mwt-package() && mwt-package_stage()
    # passes conf, conf_package, and conf_package_stage arrays to configure, and
    # users can add options through environment with e.g.
    #    MWT_BINUTILS_CONF="--some-option"
    #    MWT_GCC_STAGE1_CONF="--some-gcc-stage1-only-option"
    #    MWT_WIDL_CONF="--some-other-option"
    #    EXTRA_ECONF="--global-option" (generic naming for if not reading this)
    mwt-build() {
        if [[ ${1} == -x ]]; then
            (
                # cross-compiling, cleanup and let ./configure handle it
                edo unset AR AS CC CPP CXX DLLTOOL LD NM OBJCOPY OBJDUMP RANLIB RC STRIP
                CHOST=${CTARGET}
                filter-flags '-fuse-ld=*'
                filter-flags '-mfunction-return=thunk*' # (bgo #878849)

                # -mavx with mingw-gcc has a history of obscure issues and
                # disabling is seen as safer, e.g. `WINEARCH=win32 winecfg`
                # crashes with -march=skylake >=wine-8.10, similar issues with
                # znver4: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=110273
                append-flags -mno-avx

                strip-unsupported-flags
                mwt-build "${@:2}"
            )
	        return
        fi

        local id=${1##*/}
        local build_dir=${WORK}/${1}${2+_${2}}-build

        # econf is not allowed in src_compile and its defaults are
        # mostly unused here, so use configure directly
        local conf=( "${WORK}/${1}"/configure "${conf[@]}" )

        local -n conf_id=conf_${id} conf_id2=conf_${id}_${2}
        [[ ${conf_id@a} == *a* ]] && conf+=( "${conf_id[@]}" )
        [[ ${2} && ${conf_id2@a} == *a* ]] && conf+=( "${conf_id2[@]}" )

        local -n extra_id=MWT_${id^^}_CONF extra_id2=MWT_${id^^}_${2^^}_CONF
        conf+=( ${EXTRA_ECONF} ${extra_id} ${2+${extra_id2}} )

        #einfo "Building ${id}${2+ ${2}} in ${build_dir} ..."

        edo mkdir -p "${build_dir}"
        edo pushd "${build_dir}" >/dev/null

        edo "${conf[@]}"
        emake MAKEINFO=: V=1
        # -j1 to match bgo #906155, other packages may be fragile too
        emake -j1 MAKEINFO=: V=1 DESTDIR="${MWT_D}" install

        declare -f mwt-${id} >/dev/null && edo mwt-${id}
        declare -f mwt-${id}_${2} >/dev/null && edo mwt-${id}_${2}

        edo popd >/dev/null
    }

    # workaround race condition with out-of-source crt build (bgo #879537)
    edo mkdir -p mingw64_runtime-build/mingw-w64-crt/lib{32,64}

    # build with same ordering that crossdev would do + stage3 for pthreads
    mwt-build binutils
    mwt-build mingw64 headers
    mwt-build gcc stage1
    mwt-build -x mingw64 runtime
    mwt-build gcc stage2
    ${multilib} && mwt-build -x mingw64 libraries32
    mwt-build -x mingw64 libraries
    mwt-build gcc stage3
    mwt-build mingw64/mingw-w64-tools/widl
    # note: /could/ system-bootstrap if already installed, but gcc and
    # libraries will use the system's older mingw64 headers/static-libs
    # and make this potentially fragile without more workarounds/stages

    if ${multilib}; then
        # Like system's gcc, `x86_64-w64-mingw32-gcc -m32` can build for x86,
        # but packages expect crossdev's i686-w64-mingw32-gcc which is the same
        # just without 64bit support and would rather not build the toolchain
        # twice. Dirty but wrap to allow simple interoperability with crossdev.
        mwt-i686_wrapper() {
            edo printf "#!/usr/bin/env sh\nexec \"${prefix}/bin/${bin}\" ${*} "'"${@}"\n' \
                > ${bin32}
            edo chmod +x ${bin32}
        }
        edo pushd "${sysroot}"/bin >/dev/null
        local bin bin32
        for bin in ${CTARGET}-*; do
            bin32=${bin/x86_64-w64/i686-w64}
            case ${bin#${CTARGET}-} in
                as) mwt-i686_wrapper --32;;
                cpp|gcc|gcc-${GCC_PV%%[.-]*}|g++|widl) mwt-i686_wrapper -m32;;
                ld|ld.bfd) mwt-i686_wrapper -m i386pe;;
                windres) mwt-i686_wrapper --target=pe-i386;;
                *) edo ln -s ${bin} ${bin32};;
            esac
        done
        edo popd >/dev/null
    fi

    #einfo "Stripping ${CTARGET} static libraries ..."
    edo find "${sysroot}"/{,lib/gcc/}${CTARGET} -type f -name '*.a' \
        -exec ${CTARGET}-strip --strip-unneeded {} +
}

src_install() {
    edo mv "${MWT_D}"/* "${IMAGE}"

    edo find "${IMAGE}" -type f -name '*.la' -delete

    # remove empty directories
    edo find "${IMAGE}" -type d -empty -delete
}

