# Copyright 2023 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require go [ go_minimum_version=1.21 ]
require github [ user=docker project=buildx tag="v${PV}" ]

SUMMARY="Docker CLI plugin for extended build capabilities with BuildKit"
HOMEPAGE+=" https://www.docker.com/"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"

DEPENDENCIES="
    run:
        app-virtualization/moby[>=19.03]
"

src_compile() {
    edo go build \
        -o ${PN} \
        -trimpath \
        -ldflags "
            -linkmode=external
            -X github.com/docker/buildx/version.Version=${PV}
            -X github.com/docker/buildx/version.Package=github.com/docker/buildx
        " \
        ./cmd/buildx
}

src_test() {
    # TestGit requires a git clone
    # TestHCLWithVariablesInFunctions fails as it tries uses ${REPO} env var which we already have set
    # TestIntegration requires running dockerd
    edo go test ./... -skip "TestGit|TestHCLWithVariablesInFunctions|TestIntegration";
}

src_install() {
    exeinto /usr/$(exhost --target)/libexec/docker/cli-plugins
    doexe ${PN}

    dodoc README.md
}

