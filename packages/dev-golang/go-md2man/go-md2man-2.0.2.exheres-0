# Copyright 2017 Arnaud Lefebvre <a.lefebvre@outlook.fr>
# Distributed under the terms of the GNU General Public License v2

GITHUB_USER="cpuguy83"

require github [ user=${GITHUB_USER} tag=v${PV} ]

SUMMARY="Process markdown into man pages"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/go[>=1.11]
"

# strip usually doesn't work great for golang binaries
RESTRICT="strip"

src_prepare() {
    default

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/github.com/${GITHUB_USER}/${PN}
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/github.com/${GITHUB_USER}/${PN}/v$(ever major)
}

src_compile() {
    # required to fix build with go >= 1.13, last checked: 2.0.1
    edo export GO111MODULE=off

    edo pushd "${WORKBASE}"/build/src/github.com/${GITHUB_USER}/${PN}/v$(ever major)

    edo go fix
    edo go build \
        -o bin/${PN} \

    edo popd
}

src_install() {
    if exhost --is-native -q; then
        edo bin/${PN} -in ${PN}.1.md -out ${PN}.1
        doman ${PN}.1
    fi

    dobin bin/${PN}
}

src_test() {
    # no tests but there is a `check` target in the makefile
    # that needs a linter (which isn't packaged yet)
    :
}

